
CREATE DATABASE `sunquick` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `sunquick`;


CREATE TABLE `question` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_date` date DEFAULT NULL,
  `created_user_id` bigint(20) DEFAULT NULL,
  `last_modified_date` date DEFAULT NULL,
  `last_modified_user_id` bigint(20) DEFAULT NULL,
  `name` varchar(1000) DEFAULT NULL,
  `round` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=20;

CREATE TABLE `answer` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_date` date DEFAULT NULL,
  `created_user_id` bigint(20) DEFAULT NULL,
  `last_modified_date` date DEFAULT NULL,
  `last_modified_user_id` bigint(20) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `status` bit(1) DEFAULT NULL,
  `question_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK8frr4bcabmmeyyu60qt7iiblo` (`question_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=20;

CREATE TABLE `active_round` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `round` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=20;

CREATE TABLE `authorities` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `authority` varchar(255) DEFAULT NULL,
  `username` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKhcqd6xr5j0otd5q58nnalyseb` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=20;

CREATE TABLE `participant_answered_question` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) DEFAULT NULL,
  `question_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKcc58w4d74im48s49ukwoi7cml` (`question_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=20;

CREATE TABLE `question_answer` (
  `participant_id` bigint(20) NOT NULL,
  `answer_id` bigint(20) NOT NULL,
  UNIQUE KEY `UK_pp3ml2kmf8vlgciq2vewu4xea` (`answer_id`),
  KEY `FK4070m69qygsp5dkq7ypuoblo5` (`participant_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=20;

CREATE TABLE `result` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `mark` int(11) DEFAULT NULL,
  `send` bit(1) DEFAULT NULL,
  `user_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=20;

CREATE TABLE `user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_date` date DEFAULT NULL,
  `created_user_id` bigint(20) DEFAULT NULL,
  `last_modified_date` date DEFAULT NULL,
  `last_modified_user_id` bigint(20) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `enabled` bit(1) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `path` varchar(255) DEFAULT NULL,
  `role` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `round` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=20;



CREATE TABLE `question` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_date` date DEFAULT NULL,
  `created_user_id` bigint(20) DEFAULT NULL,
  `last_modified_date` date DEFAULT NULL,
  `last_modified_user_id` bigint(20) DEFAULT NULL,
  `name` varchar(1000) DEFAULT NULL,
  `round` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=20;

CREATE TABLE `answer` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_date` date DEFAULT NULL,
  `created_user_id` bigint(20) DEFAULT NULL,
  `last_modified_date` date DEFAULT NULL,
  `last_modified_user_id` bigint(20) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `status` bit(1) DEFAULT NULL,
  `question_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK8frr4bcabmmeyyu60qt7iiblo` (`question_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=20;