use sunquick;

SET SQL_SAFE_UPDATES = 0;

update sunquick.result set send=0;

update sunquick.question set sent=0 ;

truncate table participant_answered_question;

truncate table participant_not_answered_question;

truncate table round3answer;

truncate table result;

update sunquick.user set round=1 where role='USER';

DELETE FROM `sunquick`.`active_round`;


UPDATE `sunquick`.`user` SET `round`=null WHERE role='MANAGER';
