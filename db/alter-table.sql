ALTER TABLE `sunquick`.`user`
CHANGE COLUMN `username` `username` VARCHAR(255) NOT NULL ,
ADD UNIQUE INDEX `username_UNIQUE` (`username` ASC);
