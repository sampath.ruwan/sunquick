'use strict';

_initSocket();

function _initSocket() {

//$('#round3Submit').hide();

//alert("round 3 question");

                var options = {
                        TICKER_TOPIC : "/exchange/sunquick.gameData.exchange/"+"sunquick.round3.question"
                    },
                    socket = {
                        client: null,
                        stomp: null
                    };

                function __initialize() {
                    socket.client = new SockJS(urlQueue+"sunquick-internal/");
                    socket.stomp = Stomp.over(socket.client);
                    socket.stomp.connect({}, function() {
                        socket.stomp.subscribe(options.TICKER_TOPIC, function(data) {
                            //dont format the data here ( Format on _datamerge function );
                            _dataMerge(data);
                        });
                    });
                    socket.stomp.onclose = __reconnect; //check this MAY BE NEED TO ADD ();
                }

                function __reconnect() {
                    $timeout(function() {
                        __initialize();
                    }, 30000);
                }

                __initialize();
            }

            function _dataMerge(data) {

            var result = JSON.parse(data.body);


               //$('#round3Submit').css("display","none");

               console.log(result.message);


                if(result.message == "intro3") {

                   window.location.replace(urlQueue+"intro3/show");

                }  else if(result.message == "logout") {

                   window.location.replace(urlQueue+"login");

                } else if(result.message == "question") {

                    window.location.replace(urlQueue+"question/round3");

                } else if(result.message == "result") {

                     window.location.replace(urlQueue+"result/round/3");

                } else if(result.message == "success") {
                    $('#'+result.userId).html("");
                    $('#'+result.userId).append(result.status);
                    //$('#'+result.userId).append(result.number);
                } else if(result.message == "correct") {

                    //$('#loader').hide();
                    console.log("answer :"+result.number);

                    if(result.number == "A") {
                        $('#A').addClass("correct_answer");
                        $('#'+result.userId).addClass("correct_answer");
                    }
                    if(result.number == "B") {
                        $('#B').addClass("correct_answer");
                        $('#'+result.userId).addClass("correct_answer");
                    }
                    if(result.number == "C") {
                        $('#C').addClass("correct_answer");
                        $('#'+result.userId).addClass("correct_answer");
                    }
                    if(result.number == "D") {
                        $('#D').addClass("correct_answer");
                        $('#'+result.userId).addClass("correct_answer");
                    }

                    if (!!result.users) {
                    result.users.forEach(updateUser);
                    }
                     if(!!result.userAnswers) {

                     result.userAnswers.forEach(updateAnswer)
                     }




                    /*$('#mark').html("");
                    result.results.forEach(updateMark)*/

                } else if(result.message == "result") {

                    window.location.replace(urlQueue+"result/round/3");

                }  else if(result.message == "round3") {

                    window.location.replace(urlQueue+"question/round3");

                } else {

                    //$('.q-reply_1').innerHTML = "?";
                    $('.q-reply_1').text('?');


                    result.forEach(myFunction);

                    console.log(result);
                    console.log(result.userName);

                }
            }


            var questionId;
            var answerId;

            function myFunction(value, index, array) {

            questionId = value.id;

              console.log(value);
              console.log(questionId);
               $('#round3Submit').show();
              $('#loader').hide();
              $('#question').html("");
              $('#question').append('<h4>' + value.name + '</h4>');

              value.answers.forEach(appendAnswers,questionId);
              removeUserStyle();

            }

            function appendAnswers(value, index, array) {

            var question = this.valueOf();

            console.log("question "+question);

            console.log(value.number);

            if(value.number != null && value.number=='A') {
                $('#answers').html("");
                $('#answers').append('<div class="checkbox"><label class="form-check-label"><p id="A"><span>' + value.number + '</span><input type="radio" name="answer" class="form-check-input" onclick="selectAnswer('+value.id+','+question+');"/><label>'+value.name+'</label></p></label></div>');
                //$('#answers').append('<div class="q-answer_first" id="A"><div class="checkbox"><input type="checkbox" id="option1" onclick="selectAnswer('+value.id+','+question+');"/><label for="option1"><p><span>' + value.number + '</span>'+value.name+'</p></label></div></div>');
            }

            if(value.number != null && value.number=='B') {
                $('#answers').append('<div class="checkbox"><label class="form-check-label"><p id="B"><span>' + value.number + '</span><input type="radio" name="answer" class="form-check-input" onclick="selectAnswer('+value.id+','+question+');"/><label>'+value.name+'</label></p></label></div>');
                 //$('#answers').append('<div class="q-q-answer_second" id="B"><div class="checkbox"><input type="checkbox" id="option2" onclick="selectAnswer('+value.id+','+question+');"/><label for="option2"><p><span>' + value.number + '</span>'+value.name+'</p></label></div></div>');
            }

             if(value.number != null && value.number=='C') {

             $('#answers').append('<div class="checkbox"><label class="form-check-label"><p id="C"><span>' + value.number + '</span><input type="radio" name="answer" class="form-check-input" onclick="selectAnswer('+value.id+','+question+');"/><label>'+value.name+'</label></p></label></div>');

                  //$('#answers').append('<div class="q-answer_first" id="C"><div class="checkbox"><input type="checkbox" id="option3" onclick="selectAnswer('+value.id+','+question+');"/><label for="option3"><p><span>' + value.number + '</span>'+value.name+'</p></label></div></div>');
             }

             if(value.number != null && value.number=='D') {
                $('#answers').append('<div class="checkbox"><label class="form-check-label"><p id="D"><span>' + value.number + '</span><input type="radio" name="answer" class="form-check-input" onclick="selectAnswer('+value.id+','+question+');"/><label>'+value.name+'</label></p></label></div>');
                  //$('#answers').append('<div class="q-q-answer_second" id="D"><div class="checkbox"><input type="checkbox" id="option4" onclick="selectAnswer('+value.id+','+question+');"/><label for="option4"><p><span>' + value.number + '</span>'+value.name+'</p></label></div></div>');
             }

            }

            function selectAnswer(answer,question) {
            answerId=answer;
            questionId = question;

            console.log("select question "+questionId)
            console.log("select answer "+answerId)

            }

            function submitAnswer() {

            console.log("select question "+questionId)
            console.log("select answer "+answerId)


            var data = {}
            data["questionId"] = questionId;
            data["answerId"] = answerId;


            $.ajax({
                        type : "POST",
                        contentType : 'application/json; charset=utf-8',
                        dataType : 'json',
                        url : urlQueue+"answer/submit/round3",
                        data : JSON.stringify(data),
                        success : function(result) {
                            console.log("SUCCESS: ", result);
                            $('#alert-success').css("display", "block");
                            $("#finalize-btn").prop("disabled", true);
                            $("#message").append(result.message);
                            $("#"+result.id).append(result.number);
                            location.reload();
                        },
                        error: function(e){
                            console.log("ERROR: ", e);
                            $("#finalize-btn").prop("disabled", false);
                            //displayUsernamError(e);
                        },
                        done : function(e) {
                            console.log("DONE");
                        }
                });

            }

            function updateUser(value, index, array) {
                console.log("styling corrected answerd user")
                //$(".all#"+value.id).addClass("correct_answer");

                //$('#'+value.id).parent().addClass("animate-flicker");

                $('#'+value.id).addClass("correct_answer")
                $('#'+value.id).parent().css('background', '#fbc350');

            }


            function removeUserStyle() {

                console.log("reoving styling corrected answerd user")
                //$(".all#"+value.id).addClass("correct_answer");

                $('.q-reply_1').parent().removeClass("animate-flicker");

                 $('.q-reply_1').parent().css('background', '');

            }



            function updateAnswer(value, index, array) {

                console.log("removing styling corrected answered user id"+value.user.id);
                console.log("removing styling corrected answered answer number "+value.answer.number);
                console.log("removing styling corrected answered user"+value.id);

                $('#'+value.user.id).html("");
                $('#'+value.user.id).append(value.answer.number);


            }


            function clearcontent(elementID) {
                        document.getElementById(elementID).innerHTML = "";
                    }






