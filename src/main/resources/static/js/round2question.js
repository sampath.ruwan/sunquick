'use strict';

_initSocket();

function _initSocket() {

//alert("round 2 question");

                var options = {
                        TICKER_TOPIC : "/exchange/sunquick.gameData.exchange/"+"sunquick.round2.question"
                    },
                    socket = {
                        client: null,
                        stomp: null
                    };

                function __initialize() {
                    socket.client = new SockJS(urlQueue+"sunquick-internal/");
                    socket.stomp = Stomp.over(socket.client);
                    socket.stomp.connect({}, function() {
                        socket.stomp.subscribe(options.TICKER_TOPIC, function(data) {
                            //dont format the data here ( Format on _datamerge function );
                            _dataMerge(data);
                        });
                    });
                    socket.stomp.onclose = __reconnect; //check this MAY BE NEED TO ADD ();
                }

                function __reconnect() {
                    $timeout(function() {
                        __initialize();
                    }, 30000);
                }

                __initialize();
            }

            function _dataMerge(data) {

            console.log(data);
            console.log(data.body);

            var result = JSON.parse(data.body);

            if(result.message == "intro2") {

                window.location.replace(urlQueue+"intro2/show");

            } else if(result.message == "logout") {

                window.location.replace(urlQueue+"login");

            } else if(result.message == "question") {

                window.location.replace(urlQueue+"question/round2");

            } else if(result.message == "result") {

                window.location.replace(urlQueue+"result/round/2");

            } else if(result.message == "correct") {

                console.log("answer :"+result.number)

                if(result.number == "A") {
                $('#A').addClass("correct_answer ");
                //$('#'+result.userId).addClass("correct_answer animate-flicker");
                }
                if(result.number == "B") {
                $('#B').addClass("correct_answer ");
                //$('#'+result.userId).addClass("correct_answer animate-flicker");
                }
                if(result.number == "C") {
                $('#C').addClass("correct_answer ");
                //$('#'+result.userId).addClass("correct_answer animate-flicker");
                }
                if(result.number == "D") {
                $('#D').addClass("correct_answer");
                //$('#'+result.userId).addClass("correct_answer animate-flicker");
                }

            } else {
                result.forEach(myFunction);
                //console.log(result.userName);
            }

            }

            var questionId;
            var answerId;

            function myFunction(value, index, array) {
              console.log(value);
              $('#loader').hide();
              $('#question').html("");
              $('#question').append('<h4>' + value.name + '</h4>');
              value.answers.forEach(appendAnswers);
            }

        function appendAnswers(value, index, array) {

            console.log("appending answers "+value);

            console.log(value.number);

            if(value.number != null && value.number=='A') {
                $('#answers').html("");
                $('#answers').append('<div class="checkbox"><label class="form-check-label"><p id="A"><span>' + value.number + '</span><input type="radio" name="answer" class="form-check-input" onclick="selectAnswer('+value.id+','+question+');"/><label>'+value.name+'</label></p></label> </div>');


                }

            if(value.number != null && value.number=='B') {
                $('#answers').append('<div class="checkbox"><label class="form-check-label"><p id="B"><span>' + value.number + '</span><input type="radio" name="answer" class="form-check-input" onclick="selectAnswer('+value.id+','+question+');"/><label>'+value.name+'</label></p></label> </div>');
                //$('#answers').append('<div class="q-q-answer_second" id="B"><div class="checkbox"><input type="checkbox" id="option2" onclick="selectAnswer('+value.id+','+question+');"/><label for="option2"><p><span>' + value.number + '</span>'+value.name+'</p></label></div></div>');
            }

            if(value.number != null && value.number=='C') {

                $('#answers').append('<div class="checkbox"><label class="form-check-label"><p id="C"><span>' + value.number + '</span><input type="radio" name="answer" class="form-check-input" onclick="selectAnswer('+value.id+','+question+');"/><label>'+value.name+'</label></p></label> </div>');

                //$('#answers').append('<div class="q-answer_first" id="C"><div class="checkbox"><input type="checkbox" id="option3" onclick="selectAnswer('+value.id+','+question+');"/><label for="option3"><p><span>' + value.number + '</span>'+value.name+'</p></label></div></div>');
            }

            if(value.number != null && value.number=='D') {
                $('#answers').append('<div class="checkbox"><label class="form-check-label"><p id="D"><span>' + value.number + '</span><input type="radio" name="answer" class="form-check-input" onclick="selectAnswer('+value.id+','+question+');"/><label>'+value.name+'</label></p></label> </div>');
                //$('#answers').append('<div class="q-q-answer_second" id="D"><div class="checkbox"><input type="checkbox" id="option4" onclick="selectAnswer('+value.id+','+question+');"/><label for="option4"><p><span>' + value.number + '</span>'+value.name+'</p></label></div></div>');
            }

        }

        function selectAnswer(answer,question) {
                    answerId=answer;
                    questionId = question;

                    console.log("select question "+questionId)
                    console.log("select answer "+answerId)

                    }

                    function submitAnswer() {

                    console.log("select question "+questionId)
                    console.log("select answer "+answerId)


                    var data = {}
                    data["questionId"] = questionId;
                    data["answerId"] = answerId;


                    $.ajax({
                                type : "POST",
                                contentType : 'application/json; charset=utf-8',
                                dataType : 'json',
                                url : urlQueue+"answer/submit/round3",
                                data : JSON.stringify(data),
                                success : function(result) {
                                    console.log("SUCCESS: ", result);
                                    $('#alert-success').css("display", "block");
                                    $("#finalize-btn").prop("disabled", true);
                                    $("#message").append(result.message);
                                    $("#"+result.id).append(result.number);
                                    location.reload();
                                },
                                error: function(e){
                                    console.log("ERROR: ", e);
                                    $("#finalize-btn").prop("disabled", false);
                                    //displayUsernamError(e);
                                },
                                done : function(e) {
                                    console.log("DONE");
                                }
                        });

                    }

                    function updateUser(value, index, array) {
                        console.log("styling corrected answerd user")
                        //$(".all#"+value.id).addClass("correct_answer");

                        $('#'+value.id).parent().addClass("animate-flicker");

                        $('#'+value.id).parent().css('border', '4px solid #fbc350');

                    }


                    function removeUserStyle() {

                        console.log("reoving styling corrected answerd user")
                        //$(".all#"+value.id).addClass("correct_answer");

                        $('.q-reply_1').parent().removeClass("animate-flicker");

                        $('.q-reply_1').parent().css('border', '');

                    }



                    function updateMark(value, index, array) {

                        console.log("removing styling corrected answered user"+value.id);

                        if(value.rank == 1) {
                            $('#mark').append('<div class="activity-stream right_menu animate-flicker" ><div class="stream"><div class="stream-panel"><div class="stream-info"><img src="' + value.user.path + '" width="70" height="70"/><span>'+value.user.username+'</span></div><div class="row"><div class="stream-small col-md-6 col-sm-6 col-xs-6"><i class="fa fa-trophy"></i></div><div class="stream-small col-md-6 col-sm-6 col-xs-6"><span class="label label-primary">'+value.mark+'</span><span class="text-muted">'+value.rank+'</span></div></div></div></div>');
                        } else {
                            $('#mark').append('<div class="activity-stream right_menu" ><div class="stream"><div class="stream-panel"><div class="stream-info"><img src="' + value.user.path + '" width="70" height="70"/><span>'+value.user.username+'</span></div><div class="row"><div class="stream-small col-md-6 col-sm-6 col-xs-6"><i class="fa fa-trophy"></i></div><div class="stream-small col-md-6 col-sm-6 col-xs-6"><span class="label label-primary">'+value.mark+'</span><span class="text-muted">'+value.rank+'</span></div></div></div></div>');
                        }
                    }

