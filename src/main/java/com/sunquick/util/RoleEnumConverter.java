package com.sunquick.util;


import java.beans.PropertyEditorSupport;

/**
 * @author nuwan on 7/31/15.
 */
public class RoleEnumConverter extends PropertyEditorSupport {

    @Override
    public void setAsText(String text) throws IllegalArgumentException {
        String capitalized = text.toUpperCase();
        Role roles = Role.valueOf(capitalized);
        setValue(roles);
    }
}
