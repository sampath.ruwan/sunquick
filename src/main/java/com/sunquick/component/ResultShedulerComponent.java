package com.sunquick.component;

import com.sunquick.config.RabbitMQConfig;
import com.sunquick.domain.Answer;
import com.sunquick.domain.Result;
import com.sunquick.service.ResultService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Slf4j
@RestController
public class ResultShedulerComponent {

    @Autowired
    private RabbitTemplate rabbitMQTemplate;

    /*@Autowired
    private DirectExchange marketDataExchange;*/

    @Autowired
    private TopicExchange marketDataExchange;

    @Autowired
    private ResultService resultService;

    //@Scheduled(cron = "${cron.expression}")
    public void broadCastMarketStatus() {
        log.info("automatically runs to broadcast market status");
        try {

            List<Result> results =  resultService.findResultsBySent(Boolean.FALSE);
            if(results.size() > 0) {
                rabbitMQTemplate.convertAndSend(marketDataExchange.getName(), RabbitMQConfig.ROUTING_PATTERN_RESULT_STATUS, results);
                for (Result result:results) {
                    result.setSend(Boolean.TRUE);
                }

                resultService.saveAll(results);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
