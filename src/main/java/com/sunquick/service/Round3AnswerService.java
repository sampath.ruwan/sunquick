package com.sunquick.service;

import com.sunquick.domain.Answer;
import com.sunquick.domain.Question;
import com.sunquick.domain.Round3Answer;

import java.util.List;
import java.util.Optional;

public interface Round3AnswerService  {

    void save(Round3Answer round3Answer);

    List<Round3Answer> findByQuestionAndAnswer(Question question, Answer correctAnswer);

    List<Round3Answer> findByQuestion(Question question);

    List<Round3Answer> findAll();
}
