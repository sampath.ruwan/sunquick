package com.sunquick.service.impl;

import com.sunquick.domain.User;
import com.sunquick.repository.UserRepository;
import com.sunquick.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Transactional
    @Override
    public void save(User user) {
        log.info("user Id : {}",user.getId());

        if(user.getId() != null) {
            Optional<User> userDb = userRepository.findById(user.getId());
            if(userDb.isPresent()) {
                userDb.get().setRound(user.getRound());
                userDb.get().setUsername(user.getUsername());
                userDb.get().setEnabled(Boolean.TRUE);
                userDb.get().setRound(user.getRound());
                userDb.get().setLastModifiedDate(LocalDate.now());
                if(user.getPath() != null || StringUtils.isNoneBlank(user.getPath())) {
                    userDb.get().setPath(user.getPath());
                }
                userRepository.save(userDb.get());
            }
        } else {
            if (StringUtils.isNoneBlank(user.getPassword())) {
                user.setPassword(new BCryptPasswordEncoder().encode(user.getPassword()));
            } else {
                user.setPassword(new BCryptPasswordEncoder().encode("123456"));
            }
            user.setEnabled(Boolean.TRUE);
            userRepository.save(user);
        }
    }

    @Transactional(readOnly = true)
    @Override
    public List<User> findAll() {
        return (List<User>) userRepository.findAll();
    }

    @Override
    public Optional<User> findById(long id) {

        return userRepository.findById(id);
    }

    @Override
    public void delete(User user) {
        userRepository.delete(user);
    }

    @Override
    public Optional<User> getUserByUserName(String username) {
        return userRepository.findOneByUsername(username);
    }

    @Override
    public List<User> findByRound(Integer round) {
        return userRepository.findByRound(round);
    }

    @Override
    public List<User> saveAll(List<User> highestMarkUsers) {
        return (List<User>) userRepository.saveAll(highestMarkUsers);
    }
}
