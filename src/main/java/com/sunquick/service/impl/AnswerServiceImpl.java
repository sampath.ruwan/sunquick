package com.sunquick.service.impl;

import com.sunquick.config.RabbitMQConfig;
import com.sunquick.domain.*;
import com.sunquick.repository.AnswerRepository;
import com.sunquick.repository.ParticipantAnsweredQuestionRepository;
import com.sunquick.repository.ParticipantNotAnsweredQuestionRepository;
import com.sunquick.repository.ResultRepository;
import com.sunquick.service.AnswerService;
import com.sunquick.service.QuestionService;
import com.sunquick.util.CurrentUser;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Service
public class AnswerServiceImpl implements AnswerService {

    @Autowired
    private ParticipantAnsweredQuestionRepository participantAnsweredQuestionRepository;

    @Autowired
    private ParticipantNotAnsweredQuestionRepository participantNotAnsweredQuestionRepository;

    @Autowired
    private QuestionService questionService;

    @Autowired
    private ResultRepository resultRepository;

    @Autowired
    private RabbitTemplate rabbitMQTemplate;

    /*@Autowired
    private DirectExchange marketDataExchange;*/

    @Autowired
    private TopicExchange marketDataExchange;

    @Autowired
    private AnswerRepository answerRepository;

    @Autowired
    private AnswerService answerService;

    //@Async
    /*When submitting answer find logged in user. create two list as answered question and not answered question
    * then iterate answered question list and picked db question and pick the correct answer. According to the correct answer
    * user get markes and user will get minus marks for wrong answer*/
    @Transactional
    @Override
    public Result save(List<Question> questions) {

        final CurrentUser currentUser = (CurrentUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        /*question list devide into two list as answered question and unanswered question*/
        List<Question> answeredQuestions = new ArrayList<>();
        List<Question> unansweredQuestions = new ArrayList<>();

        List<ParticipantAnsweredQuestion> participantAnsweredQuestions = new ArrayList<>();
        List<ParticipantNotAnsweredQuestion> participantNotAnsweredQuestions = new ArrayList<>();

        int userMark = 0;
        Result result = new Result();

        if(questions != null && questions.size() > 0) {

            for(Question question:questions) {

                //Boolean answered = question.getAnswers().stream().anyMatch(answer -> answer.getIsSet());

                log.info(question.getId()+" : is this question answered: {}",question.getAnswers());

                //check question is answerd or not
                if (question.getAnswers() != null && question.getAnswers().size() > 0) {
                    answeredQuestions.add(question);
                    ParticipantAnsweredQuestion participantAnsweredQuestion = new ParticipantAnsweredQuestion();
                    participantAnsweredQuestion.setQuestion(question);
                    Optional<Answer> answer = answerRepository.findById(question.getAnswers().get(0).getId());
                    if(answer.isPresent()) {
                        participantAnsweredQuestion.setAnswer(answer.get());
                    }
                    participantAnsweredQuestion.setUser(currentUser.getUser());
                    participantAnsweredQuestion.setRound(currentUser.getUser().getRound());
                    participantAnsweredQuestions.add(participantAnsweredQuestion);
                } else {
                    ParticipantNotAnsweredQuestion participantNotAnsweredQuestion = new ParticipantNotAnsweredQuestion();
                    participantNotAnsweredQuestion.setQuestion(question);
                    //participantNotAnsweredQuestion.setAnswers(question.getAnswers());
                    participantNotAnsweredQuestion.setUser(currentUser.getUser());
                    participantNotAnsweredQuestion.setRound(currentUser.getUser().getRound());
                    participantNotAnsweredQuestions.add(participantNotAnsweredQuestion);
                    //unansweredQuestions.add(question);
                }
            }

            if(answeredQuestions.size() > 0) {

                log.info("answeredQuestions.size {}",answeredQuestions.size());

                for(Question question:answeredQuestions) {

                    Question questionPersisted = questionService.findQuestionById(question.getId());

                    log.info("DB question {}",question);

                    if(questionPersisted != null) {

                        log.info("All answers {}",questionPersisted.getAnswers());

                        Answer correctAnswer = answerService.answerByQuestion(questionPersisted);
                        //List<Answer> correctAnswer = questionPersisted.getAnswers().stream().filter(answer -> answer.getStatus().equals(Boolean.TRUE)).collect(Collectors.toList());

                        log.info("correctAnswer {}",correctAnswer.getName());
                        log.info("participant answer {}",question.getAnswers().get(0).getName());

                        log.info("userMarks {}",userMark);
                        if(question.getAnswers().get(0).getId().equals(correctAnswer.getId())) {
                            userMark++;
                            log.info("plus userMark {}",userMark);

                        } /*else {
                            userMark--;
                            log.info("minus userMark {}",userMark);
                        }*/
                    } else {

                        log.info("invalid question id");
                    }

                }

                if(participantAnsweredQuestions != null && participantAnsweredQuestions.size() > 0) {
                    participantAnsweredQuestionRepository.saveAll(participantAnsweredQuestions);
                }

                if(participantNotAnsweredQuestions != null && participantNotAnsweredQuestions.size() > 0) {
                    participantNotAnsweredQuestionRepository.saveAll(participantNotAnsweredQuestions);
                }

                log.info("current user {}",currentUser);
                log.info("current user name{}",currentUser.getUsername());
                result.setMark(userMark);
                result.setUser(currentUser.getUser());
                result.setRound(currentUser.getUser().getRound());
                result.setSend(Boolean.FALSE);
                Result persistedResult = resultRepository.save(result);
                //rabbitMQTemplate.convertAndSend(marketDataExchange.getName(), RabbitMQConfig.ROUTING_PATTERN_RESULT_STATUS, persistedResult);

                log.info("send result to {}", RabbitMQConfig.ROUTING_PATTERN_RESULT_STATUS);


            } else {
                log.info("not answered any question");
            }
        }

        return result;
    }

    @Transactional(readOnly = true)
    @Override
    public Answer findById(Long answerId) {

        Optional<Answer> answer = answerRepository.findById(answerId);
        if (answer.isPresent()) {
            return answer.get();
        } else {
            return null;
        }
    }

    @Transactional(readOnly = true)
    @Override
    public Answer answerByQuestion(Question question) {
        return answerRepository.findByQuestionAndStatus(question,Boolean.TRUE);
    }
}
