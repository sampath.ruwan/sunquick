package com.sunquick.service.impl;

import com.sunquick.domain.ActiveRound;
import com.sunquick.domain.Answer;
import com.sunquick.domain.Question;
import com.sunquick.repository.QuestionRepository;
import com.sunquick.service.QuestionService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class QuestionServiceImpl implements QuestionService {

    @Autowired
    private QuestionRepository questionRepository;

    @Transactional
    @Override
    public Question save(Question question) {

        log.info("question id : {}",question.getId());

        if(question.getAnswers() != null && question.getAnswers().size() > 0) {

            log.info("answer size : {}",question.getAnswers().size());

            for(Answer answer:question.getAnswers()) {

                log.info("answer : {}",answer.getId());

                answer.setQuestion(question);
            }
        }
        return questionRepository.save(question);
    }

    @Transactional(readOnly = true)
    @Override
    public List<Question> findAll() {
        return (List<Question>) questionRepository.findAll();
    }

    @Transactional(readOnly = true)
    @Override
    public Optional<Question> findById(long id) {
        Optional<Question> question = questionRepository.findById(id);
        if(question != null) {
            question.get().getAnswers().size();
        }
        return question;
    }

    @Transactional
    @Override
    public void delete(Question question) {
        questionRepository.delete(question);
    }

    @Transactional(readOnly = true)
    @Override
    public List<Question> questionsByRound(int round) {
        List<Question> questions = questionRepository.findByRoundAndSent(round,Boolean.FALSE);
        for (Question question:questions) {
            question.getAnswers().size();
        }
        return questions;
    }


    @Transactional(readOnly = true)
    @Override
    public Question findQuestionById(Long id) {
        Optional<Question> question = questionRepository.findById(id);
        if(question.isPresent()) {
            question.get().getAnswers().size();
            return question.get();
        } else {
            return null;
        }
    }

    @Transactional
    @Override
    public void saveAll(List<Question> questionsToSend) {
        questionRepository.saveAll(questionsToSend);
    }

    @Transactional
    @Override
    public Question findByRound(Integer round) {
        Question question = questionRepository.findLastSendQuestionByRound(round);
        if(question != null) {
            question.getAnswers().size();
        }
        return question;
    }
}
