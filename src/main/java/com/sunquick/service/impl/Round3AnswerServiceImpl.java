package com.sunquick.service.impl;

import com.sunquick.domain.Answer;
import com.sunquick.domain.Question;
import com.sunquick.domain.Round3Answer;
import com.sunquick.repository.Round3AnswerRepository;
import com.sunquick.service.Round3AnswerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class Round3AnswerServiceImpl implements Round3AnswerService {

    @Autowired
    private Round3AnswerRepository round3AnswerRepository;

    @Transactional
    @Override
    public void save(Round3Answer round3Answer) {
        round3AnswerRepository.save(round3Answer);
    }

    @Transactional(readOnly = true)
    @Override
    public List<Round3Answer> findByQuestionAndAnswer(Question question, Answer correctAnswer) {
        return round3AnswerRepository.findByQuestionAndAnswer(question,correctAnswer);
    }

    @Transactional(readOnly = true)
    @Override
    public List<Round3Answer> findByQuestion(Question question) {
        return round3AnswerRepository.findByQuestion(question);
    }

    @Override
    public List<Round3Answer> findAll() {
        return (List<Round3Answer>) round3AnswerRepository.findAll();
    }
}
