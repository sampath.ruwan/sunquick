package com.sunquick.service.impl;


import com.sunquick.domain.User;
import com.sunquick.service.UserDetailsService;
import com.sunquick.service.UserService;
import com.sunquick.util.CurrentUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * @author nuwan on 7/29/15.
 */
@Service("userDetailsService")
public class CurrentUserDetailsService implements UserDetailsService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CurrentUserDetailsService.class);

    private final UserService userService;

    @Autowired
    public CurrentUserDetailsService(UserService userService) {
        this.userService = userService;
    }

    @Override
    public CurrentUser loadUserByUsername(String username) throws UsernameNotFoundException {
        LOGGER.debug("Authenticating user with user name={}", username.replaceFirst("@.*", "@***"));
        User user = userService.getUserByUserName(username)
                .orElseThrow(() -> new UsernameNotFoundException(String.format("User with email=%s was not found", username)));

        return new CurrentUser(user);
    }
}
