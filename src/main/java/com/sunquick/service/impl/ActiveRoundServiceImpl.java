package com.sunquick.service.impl;

import com.sunquick.domain.ActiveRound;
import com.sunquick.repository.ActiveRoundRepository;
import com.sunquick.service.ActiveRoundService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class ActiveRoundServiceImpl implements ActiveRoundService {

    @Autowired
    private ActiveRoundRepository activeRoundRepository;

    @Transactional
    @Override
    public void save(ActiveRound activeRound) {
        List<ActiveRound> activeRounds = (List<ActiveRound>) activeRoundRepository.findAll();

        if(activeRounds != null && activeRounds.size() > 0) {
            ActiveRound activeRoundDb = activeRounds.get(0);
            activeRoundDb.setRound(activeRound.getRound());
            activeRoundRepository.save(activeRoundDb);
        } else {
            activeRoundRepository.save(activeRound);
        }

    }

    @Transactional(readOnly = true)
    @Override
    public List<ActiveRound> findAll() {
        return (List<ActiveRound>) activeRoundRepository.findAll();
    }

    @Transactional(readOnly = true)
    @Override
    public Optional<ActiveRound> findById(long id) {
        return activeRoundRepository.findById(id);
    }

    @Transactional
    @Override
    public void delete(ActiveRound round) {
        activeRoundRepository.delete(round);
    }
}
