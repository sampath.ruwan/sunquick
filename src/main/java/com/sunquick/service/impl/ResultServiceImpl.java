package com.sunquick.service.impl;

import com.sunquick.domain.Result;
import com.sunquick.domain.User;
import com.sunquick.repository.ResultRepository;
import com.sunquick.service.ResultService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.beans.Transient;
import java.util.List;

@Service
public class ResultServiceImpl implements ResultService {

    @Autowired
    private ResultRepository resultRepository;


    @Transactional(readOnly = true)
    @Override
    public List<Result> findResults() {
        return (List<Result>) resultRepository.findAll();
    }

    @Transactional
    @Override
    public void saveAll(List<Result> results) {

        resultRepository.saveAll(results);

    }

    @Transactional(readOnly = true)
    @Override
    public List<Result> findResultsBySent(Boolean aFalse) {
        return (List<Result>) resultRepository.findBySendOrderByMarkDesc(aFalse);
    }
    @Transactional(readOnly = true)
    @Override
    public Result findByUserAndRound(User user, int round) {
        return resultRepository.findByUserAndRound(user,round);
    }

    @Transactional(readOnly = true)
    @Override
    public List<Result> findByRound(int round) {
        return resultRepository.findByRoundOrderByMarkDesc(round);
    }
    @Transactional
    @Override
    public void delete() {
        resultRepository.deleteAll();
    }

    @Transactional
    @Override
    public void save(Result result) {
        resultRepository.save(result);
    }
}
