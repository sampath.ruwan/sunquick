package com.sunquick.service;

import com.sunquick.domain.ActiveRound;
import com.sunquick.domain.Question;

import java.util.List;
import java.util.Optional;

public interface QuestionService {
    
    Question save(Question question);

    List<Question> findAll();

    Optional<Question> findById(long id);

    void delete(Question url);

    List<Question> questionsByRound(int round);

    Question findQuestionById(Long id);

    void saveAll(List<Question> questionsToSend);

    Question findByRound(Integer round);
}
