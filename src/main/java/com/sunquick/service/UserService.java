package com.sunquick.service;

import com.sunquick.domain.User;

import java.util.List;
import java.util.Optional;

public interface UserService {

    void save(User user);

    List<User> findAll();

    Optional<User> findById(long id);

    void delete(User user);

    Optional<User> getUserByUserName(String email);

    List<User> findByRound(Integer round);

    List<User> saveAll(List<User> highestMarkUsers);
}
