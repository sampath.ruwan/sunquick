package com.sunquick.service;

import com.sunquick.domain.Answer;
import com.sunquick.domain.Question;
import com.sunquick.domain.Result;

import java.util.List;
import java.util.Optional;

public interface AnswerService {

    Result save(List<Question> questions);

    Answer findById(Long answerId);

    Answer answerByQuestion(Question question);
}
