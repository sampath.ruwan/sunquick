package com.sunquick.service;

import com.sunquick.domain.Result;
import com.sunquick.domain.User;

import java.util.List;

public interface ResultService {
    List<Result> findResults();

    void saveAll(List<Result> results);

    List<Result> findResultsBySent(Boolean aFalse);

    Result findByUserAndRound(User user, int i);

    List<Result> findByRound(int i);

    void delete();

    void save(Result result);
}
