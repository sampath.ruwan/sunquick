package com.sunquick.service;

import com.sunquick.domain.ActiveRound;

import java.util.List;
import java.util.Optional;

public interface ActiveRoundService {
    void save(ActiveRound activeRound);

    List<ActiveRound> findAll();

    Optional<ActiveRound> findById(long id);

    void delete(ActiveRound round);
}
