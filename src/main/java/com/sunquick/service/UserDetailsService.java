package com.sunquick.service;


import com.sunquick.util.CurrentUser;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

/**
 * @author nuwan on 7/29/15.
 */
public interface UserDetailsService extends org.springframework.security.core.userdetails.UserDetailsService{
    CurrentUser loadUserByUsername(String username) throws UsernameNotFoundException;

}
