package com.sunquick.repository;

import com.sunquick.domain.ActiveRound;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ActiveRoundRepository extends CrudRepository<ActiveRound,Long> {
}
