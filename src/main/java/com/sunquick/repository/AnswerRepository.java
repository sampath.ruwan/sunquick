package com.sunquick.repository;

import com.sunquick.domain.Answer;
import com.sunquick.domain.Question;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AnswerRepository extends CrudRepository<Answer,Long> {
    Answer findByQuestionAndStatus(Question question, Boolean aTrue);
}
