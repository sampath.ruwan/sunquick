package com.sunquick.repository;

import com.sunquick.domain.Result;
import com.sunquick.domain.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ResultRepository extends CrudRepository<Result,Long> {


    List<Result> findBySendOrderByMarkDesc(Boolean aFalse);

    Result findByUserAndRound(User user, int round);

    List<Result> findByRound(int round);

    List<Result> findByRoundOrderByMarkDesc(int round);

    void deleteByRound(int round);
}
