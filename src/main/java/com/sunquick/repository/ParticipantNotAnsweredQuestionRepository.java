package com.sunquick.repository;

import com.sunquick.domain.ParticipantNotAnsweredQuestion;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ParticipantNotAnsweredQuestionRepository extends CrudRepository<ParticipantNotAnsweredQuestion,Long> {
}
