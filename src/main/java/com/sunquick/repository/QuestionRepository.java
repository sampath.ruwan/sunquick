package com.sunquick.repository;

import com.sunquick.domain.Question;
import org.aspectj.weaver.patterns.TypePatternQuestions;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface QuestionRepository  extends CrudRepository<Question,Long> {

    List<Question> findByRound(int round);

    List<Question> findByRoundAndSent(int round, Boolean aFalse);

    @Query(value= "select * from sunquick.question where sent_time = (select max(sent_time) from sunquick.question where round=?) and sent=1 ", nativeQuery = true)
    Question findLastSendQuestionByRound(Integer round);
}
