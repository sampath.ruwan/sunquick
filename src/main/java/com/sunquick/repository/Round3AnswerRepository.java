package com.sunquick.repository;


import com.sunquick.domain.Answer;
import com.sunquick.domain.Question;
import com.sunquick.domain.Round3Answer;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface Round3AnswerRepository extends CrudRepository<Round3Answer,Long> {

    List<Round3Answer> findByQuestionAndAnswer(Question question, Answer correctAnswer);

    List<Round3Answer> findByQuestion(Question question);
}
