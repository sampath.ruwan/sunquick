package com.sunquick.repository;

import com.sunquick.domain.ParticipantAnsweredQuestion;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ParticipantAnsweredQuestionRepository extends CrudRepository<ParticipantAnsweredQuestion,Long> {
}
