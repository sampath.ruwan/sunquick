package com.sunquick.config;

import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitMQConfig {

    private static final String CSE_MARKET_DATA_EXCHANGE_NAME = "sunquick.gameData.exchange";

    @Value("${spring.rabbitmq.producer.host}")
    public String RABBITMQ_PRODUCER_HOST;

    @Value("${spring.rabbitmq.producer.port}")
    public String RABBITMQ_PRODUCER_PORT;

    @Value("${spring.rabbitmq.producer.username}")
    public String RABBITMQ_PRODUCER_USERNAME;

    @Value("${spring.rabbitmq.producer.password}")
    public String RABBITMQ_PRODUCER_PASSWORD;

    @Value("${spring.rabbitmq.cache.channel.size}")
    public String RABBITMQ_PRODUCER_CHANNEL_SIZE;

    public static final String ROUTING_PATTERN_RESULT_STATUS = "sunquick.result.status";
    public static final String ROUTING_PATTERN_ROUND2_QUESTION = "sunquick.round2.question";
    public static final String ROUTING_PATTERN_ROUND3_QUESTION = "sunquick.round3.question";
    public static final String ROUTING_PATTERN_TIMER = "sunquick.timer";
    public static final String ROUTING_PATTERN_ROUND1_RESULT = "sunquick.round1.result";


    @Bean
    public ConnectionFactory rabbitMQConnectionFactory() {
        CachingConnectionFactory factory = new CachingConnectionFactory();
        factory.setHost(RABBITMQ_PRODUCER_HOST);
        factory.setPort(Integer.parseInt(RABBITMQ_PRODUCER_PORT));
        factory.setUsername(RABBITMQ_PRODUCER_USERNAME);
        factory.setPassword(RABBITMQ_PRODUCER_PASSWORD);
        factory.setChannelCacheSize(Integer.parseInt(RABBITMQ_PRODUCER_CHANNEL_SIZE));
        return factory;
    }


    @Bean
    public RabbitTemplate rabbitMQTemplate() {
        RabbitTemplate template = new RabbitTemplate(rabbitMQConnectionFactory());
        template.setMessageConverter(jsonMessageConverter());
        template.setExchange(CSE_MARKET_DATA_EXCHANGE_NAME);
        return template;
    }

    /*@Bean
    public DirectExchange marketDataExchange() {
        return new DirectExchange(CSE_MARKET_DATA_EXCHANGE_NAME, true, false);
    }*/

    @Bean
    public TopicExchange marketDataExchange() {
        return new TopicExchange(CSE_MARKET_DATA_EXCHANGE_NAME, true, false);
    }

    @Bean
    public MessageConverter jsonMessageConverter() {
        return new Jackson2JsonMessageConverter();
    }
}
