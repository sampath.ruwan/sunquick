package com.sunquick.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;

/**
 * @author nuwan on 10/12/15.
 * @Configuration to indicate that it is a Spring configuration class.
 * @EnableWebSocketMessageBroker enables WebSocket message handling, backed by a message broker.
 * Cross-Origin Request Blocked: The Same Origin Policy
 * set to access all origing but we can change it to specific domains
 */
@Configuration
@EnableWebSocketMessageBroker
public class WebSocketStompConfig implements WebSocketMessageBrokerConfigurer {

    private static final Logger LOGGER = LoggerFactory.getLogger(WebSocketStompConfig.class);

    @Value("${spring.rabbitmq.consumer.host}")
    public String RABBITMQ_CONSUMER_HOST;

    @Value("${spring.rabbitmq.consumer.port}")
    public String RABBITMQ_CONSUMER_PORT;

    @Value("${spring.rabbitmq.consumer.username}")
    public String RABBITMQ_CONSUMER_USERNAME;

    @Value("${spring.rabbitmq.consumer.password}")
    public String RABBITMQ_CONSUMER_PASSWORD = "";

    @Override
    public void registerStompEndpoints(StompEndpointRegistry registry) {
        try {
            registry.addEndpoint("/sunquick-internal").setAllowedOrigins("*").withSockJS();
        } catch (Exception e) {
            LOGGER.error("Exception in registering user to STOMP channel : {}", e.getMessage());
        }
    }

    @Override
    public void configureMessageBroker(MessageBrokerRegistry registry) {
        try {
            registry.enableStompBrokerRelay("/direct", "/queue", "/exchange","/topic")
                    .setRelayHost(RABBITMQ_CONSUMER_HOST)
                    .setRelayPort(Integer.parseInt(RABBITMQ_CONSUMER_PORT))
                    .setClientLogin(RABBITMQ_CONSUMER_USERNAME)
                    .setClientPasscode(RABBITMQ_CONSUMER_PASSWORD);

            registry.setApplicationDestinationPrefixes("/app");
        } catch (Exception e) {
            LOGGER.error("Exception in configuring message broker : {}", e.getMessage());
        }
    }
}
