package com.sunquick.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class MvcConfig implements WebMvcConfigurer {

    public void addViewControllers(ViewControllerRegistry registry) {
        //registry.addViewController("/home").setViewName("base");
        //registry.addViewController("/").setViewName("home");
        //registry.addViewController("/hello").setViewName("hello");
        registry.addViewController("/error").setViewName("error");
        registry.addViewController("/login").setViewName("login");
        registry.addViewController("user/register").setViewName("register");
        registry.addViewController("/participant").setViewName("participant");
        registry.addViewController("/participants").setViewName("participants");
        //registry.addViewController("/result").setViewName("result/result");
        registry.addViewController("question/round2").setViewName("question/round2");

        registry.addViewController("/success").setViewName("success");
        registry.addViewController("intro2/show").setViewName("question/intro2");
        registry.addViewController("intro3/show").setViewName("question/intro3");
        //registry.addViewController("/intro").setViewName("intro");
        //registry.addViewController("/answer").setViewName("answer/create");
        //registry.addViewController("/answers").setViewName("answer/list");
    }

    /*@Bean
    public PasswordEncoder passwordEncoder() {
        //return new BCryptPasswordEncoder();
        return PasswordEncoderFactories.createDelegatingPasswordEncoder();
    }*/

    @Override
    public void addResourceHandlers(final ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/uploads/**").addResourceLocations("file:uploads/");
    }
}
