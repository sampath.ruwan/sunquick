package com.sunquick.controller;

import com.sunquick.config.RabbitMQConfig;
import com.sunquick.domain.ActiveRound;
import com.sunquick.dto.response.TimerResponse;
import com.sunquick.service.ActiveRoundService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;

@Slf4j
@Controller
@RequestMapping("/logout")
public class LogoutController {

    @Autowired
    private RabbitTemplate rabbitMQTemplate;

    @Autowired
    private TopicExchange marketDataExchange;

    @Autowired
    private ActiveRoundService activeRoundService;

    @GetMapping("/")
    public String logoutUser(Model model, RedirectAttributes redir) {

        TimerResponse response = new TimerResponse();
        response.setMessage("logout");

        try {
            List<ActiveRound> rounds = activeRoundService.findAll();
            log.info("active round: {} ",rounds.get(0).getRound());
            if(rounds.get(0).getRound() == 1) {
                rabbitMQTemplate.convertAndSend(marketDataExchange.getName(), RabbitMQConfig.ROUTING_PATTERN_ROUND1_RESULT, response);
                log.info("message send to : {} ",RabbitMQConfig.ROUTING_PATTERN_ROUND1_RESULT);
            } else if(rounds.get(0).getRound() == 2) {
                rabbitMQTemplate.convertAndSend(marketDataExchange.getName(), RabbitMQConfig.ROUTING_PATTERN_ROUND2_QUESTION, response);
                log.info("message send to : {} ",RabbitMQConfig.ROUTING_PATTERN_ROUND2_QUESTION);
            } else if(rounds.get(0).getRound() == 3) {
                rabbitMQTemplate.convertAndSend(marketDataExchange.getName(), RabbitMQConfig.ROUTING_PATTERN_ROUND3_QUESTION, response);
                log.info("message send to : {} ",RabbitMQConfig.ROUTING_PATTERN_ROUND3_QUESTION);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return "redirect:"+"/";
    }

}
