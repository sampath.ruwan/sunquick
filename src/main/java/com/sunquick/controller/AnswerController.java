package com.sunquick.controller;

import com.sunquick.config.RabbitMQConfig;
import com.sunquick.domain.*;
import com.sunquick.dto.request.CorrectAnswerQuestionRequest;
import com.sunquick.dto.response.QuestionAnswerResponse;
import com.sunquick.dto.response.Round3AnswerResponse;
import com.sunquick.modelmapper.ModelMapper;
import com.sunquick.service.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Controller
@RequestMapping("/answer/")
public class AnswerController {

    @Autowired
    private AnswerService answerService;

    @Autowired
    private ResultService resultService;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private RabbitTemplate rabbitMQTemplate;

    @Autowired
    private TopicExchange marketDataExchange;

    @Autowired
    private QuestionService questionService;

    @Autowired
    private ActiveRoundService activeRoundService;

    @Autowired
    private Round3AnswerService round3AnswerService;

    @GetMapping("create")
    public String showCreateForm(Model model) {
        model.addAttribute("answer", new Answer());
        return "answer/create";
    }


    /*First Round answer submit*/
    @PostMapping("submit")
    public String submit(@ModelAttribute("questionAnswerResponse") QuestionAnswerResponse questionAnswerResponse, BindingResult result, Model model, RedirectAttributes redir) {

        log.info("request {}",questionAnswerResponse);

        List<Question> questions = modelMapper.map(questionAnswerResponse.questions,Question.class);

        answerService.save(questions);

        redir.addFlashAttribute("error", true);
        redir.addFlashAttribute("errorMessage", "Answer submit success");

        return "redirect:"+"/logout";
    }


    /*@PostMapping("submit/round3")
    public String submitRound3(@ModelAttribute("questionAnswerResponse") QuestionRound3Response questionAnswerResponse, BindingResult result, Model model, RedirectAttributes redir) {

        log.info("request round3 {}",questionAnswerResponse);

        final CurrentUser currentUser = (CurrentUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        log.info("current user = {}",currentUser.getUsername());
        log.info("current user role = {}",currentUser.getRole());

        //answerService.save(questionAnswerResponse.getQuestions());

        redir.addFlashAttribute("error", true);
        redir.addFlashAttribute("errorMessage", "Answer submit success");

        return "redirect:"+"/success";
    }*/


    @GetMapping("correct")
    public String showCorrectAnswer(Model model) {

        List<ActiveRound> rounds = activeRoundService.findAll();

        log.info("active round {}",rounds.get(0).getRound());

        Question question = questionService.findByRound(rounds.get(0).getRound());

        CorrectAnswerQuestionRequest correctAnswer = modelMapper.map(question,CorrectAnswerQuestionRequest.class);

        log.info("correctAnswer {}",correctAnswer);

        model.addAttribute("correctAnswer", correctAnswer);

        return "answer/correct";
    }


    @PostMapping("correct")
    public String submitCorrectAnswer(@ModelAttribute("correctAnswer") CorrectAnswerQuestionRequest correctAnswer,Model model, RedirectAttributes redir) {

        log.info("question {}",correctAnswer);

        Question questionDb = questionService.findQuestionById(correctAnswer.getId());
        //get correct answer
        Answer correctAnswerDb = answerService.answerByQuestion(questionDb);

        log.info("answer {}",correctAnswerDb);

        Round3AnswerResponse response = new Round3AnswerResponse();
        response.setMessage("correct");

        if(correctAnswer != null) {
            response.setAnswerId(correctAnswerDb.getId());
            response.setNumber(correctAnswerDb.getNumber());
        }

        //highlight correct answer give users
        List<Round3Answer> round3Answers = round3AnswerService.findByQuestionAndAnswer(questionDb,correctAnswerDb);

        //display user answer number
        List<Round3Answer> round3AnswersByQuestion = round3AnswerService.findByQuestion(questionDb);

        if(round3AnswersByQuestion!= null && round3AnswersByQuestion.size()> 0) {
            List<Round3AnswerResponse.UserAnswerData> usersWiseAnswesData = modelMapper.map(round3AnswersByQuestion,Round3AnswerResponse.UserAnswerData.class);
            response.setUserAnswers(usersWiseAnswesData);

        }

        log.info("round3Answer {}",round3Answers);

        if(round3Answers != null && round3Answers.size() > 0) {
            //list of users given correct answer
            List<User> users = new ArrayList<>();
            for (Round3Answer round3Answer :round3Answers) {
                users.add(round3Answer.getUser());
            }

            if(users != null && users.size() > 0) {
                List<Round3AnswerResponse.UserData> usersData = modelMapper.map(users,Round3AnswerResponse.UserData.class);
                response.setUsers(usersData);
            }
        }

        //List<Round3AnswerResponse.UserAnswerData> userAnswerData =

        //Get users marks
        List<Result> results = calculateMarkOfUser(questionDb,correctAnswerDb);

        int rank =1;

        int markFirst = 0;

        for (Result result:results) {

            if(markFirst != 0) {
                if(result.getMark() == markFirst) {
                    result.setRank(rank);
                } else {
                    rank++;
                    result.setRank(rank);
                }
            } else {
                result.setRank(rank);
                markFirst = result.getMark();
            }
            log.info("mark {}",result.getMark());

        }

        List<Round3AnswerResponse.ResultData> resultsData = modelMapper.map(results,Round3AnswerResponse.ResultData.class);
        response.setResults(resultsData);

        log.info("response roun3 with result {}",response);

        try {
            List<ActiveRound> rounds = activeRoundService.findAll();
            if(rounds.get(0).getRound() == 2) {
                rabbitMQTemplate.convertAndSend(marketDataExchange.getName(), RabbitMQConfig.ROUTING_PATTERN_ROUND2_QUESTION, response);

            } else if(rounds.get(0).getRound() == 3) {
                rabbitMQTemplate.convertAndSend(marketDataExchange.getName(), RabbitMQConfig.ROUTING_PATTERN_ROUND3_QUESTION, response);

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        redir.addFlashAttribute("error", true);
        redir.addFlashAttribute("errorMessage", "Correct answer submit success");

        return "redirect:"+"/success";
    }


    private List<Result> calculateMarkOfUser(Question question,Answer correctAnswer) {
        //calculate mark from
        //Get All Round 3 answers by question
        List<Round3Answer> allAnswers = round3AnswerService.findByQuestion(question);

        List<Result> results = new ArrayList<>();

        //iterate answers
        for (Round3Answer round3Answer : allAnswers) {

            if(round3Answer.getAnswer() != null) {

                Result result = resultService.findByUserAndRound(round3Answer.getUser(),3);

                log.info("result ",round3Answer.getUser());

                if(round3Answer.getAnswer().getId().equals(correctAnswer.getId())) {
                    log.info("correct answered user ",round3Answer.getUser());

                    if(result == null) {
                        result = new Result();
                        result.setMark(10);
                        result.setUser(round3Answer.getUser());
                        result.setRound(3);
                    } else {
                        result.setMark(result.getMark() + 10);
                    }
                } else {
                    //wrong answer
                    if(result == null) {
                        result = new Result();
                        result.setMark(-10);
                        result.setUser(round3Answer.getUser());
                        result.setRound(3);
                    } else {
                        result.setMark(result.getMark() - 10);
                    }

                }

                results.add(result);

            }
        }
        if(results != null && results.size() > 0) {
            resultService.saveAll(results);
        }

        //find result by round
        return resultService.findByRound(3);
    }

}

