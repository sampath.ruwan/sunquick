package com.sunquick.controller;

import com.sunquick.config.RabbitMQConfig;
import com.sunquick.domain.ActiveRound;
import com.sunquick.domain.Question;
import com.sunquick.domain.User;
import com.sunquick.dto.response.Round3AnswerResponse;
import com.sunquick.dto.response.TimerResponse;
import com.sunquick.service.ActiveRoundService;
import com.sunquick.service.impl.ActiveRoundServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

@Slf4j
@Controller
@RequestMapping("/round")
public class RoundController {

    @Autowired
    private ActiveRoundService activeRoundService;



    @GetMapping("/")
    public String viewRoundForm(Model model) {
        model.addAttribute("activeRound", new ActiveRound());
        return "round/round";
    }


    @PostMapping("/")
    public String register(@Valid ActiveRound activeRound, BindingResult result, Model model) {
        log.info("Registering user {}",activeRound);

        log.info("result.hasErrors {}",result.hasErrors());

        if (result.hasErrors()) {
            return "activeRound";
        }
        activeRoundService.save(activeRound);

        //save to database

        return "redirect:list";
    }

    @GetMapping("/list")
    public String list(Model model) {
        log.info("list:view ");
        model.addAttribute("rounds", activeRoundService.findAll());
        return "round/list";
    }

    @GetMapping("delete/{id}")
    public String delete(@PathVariable("id") long id, Model model) {
        ActiveRound round = activeRoundService.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid Id:" + id));
        activeRoundService.delete(round);
        model.addAttribute("rounds", activeRoundService.findAll());
        return "round/list";
    }



}
