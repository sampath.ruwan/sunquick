package com.sunquick.controller;

import com.sunquick.config.RabbitMQConfig;
import com.sunquick.domain.*;
import com.sunquick.dto.response.Round3AnswerResponse;
import com.sunquick.dto.response.TimerResponse;
import com.sunquick.service.*;
import com.sunquick.util.Constance;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Slf4j
@Controller
@RequestMapping("/result/")
public class ResultController {

    @Autowired
    private ResultService resultService;

    @Autowired
    private UserService userService;

    @Autowired
    private ActiveRoundService activeRoundService;

    @Autowired
    private Round3AnswerService round3answerService;

    @Autowired
    private AnswerService answerService;

    @Autowired
    private QuestionService questionService;

    @Autowired
    private RabbitTemplate rabbitMQTemplate;

    @Autowired
    private TopicExchange marketDataExchange;


    @GetMapping("create")
    public String createResultForm(Model model) {
        model.addAttribute("result", new Result());
        model.addAttribute("users", userService.findByRound(2));
        return "result/create";
    }

    @PostMapping("create")
    public String submitResultForm(@Valid Result result, Model model) {
        log.info("Registering user {}",result);
        resultService.save(result);
        return "result/round2";
    }

    /*Are we going to show all users marks or highest marks user only ? all*/
    @GetMapping("round/{round}")
    public String viewResult(@PathVariable("round") Integer round ,Model model) {
        log.info("result viewing for round..............{}",round);
        int rank =1;
        List<User> highestMarkUsers = new ArrayList<>();
        List<Result> highestMarkResults =  new ArrayList<>();

        List<Result> results =  resultService.findByRound(round);

        List<ActiveRound> activeRounds = activeRoundService.findAll();

        log.info("round: {} ",round+" : result: {} ",results);

        if(results != null && results.size() > 0) {

            for (Result result:results) {

                result.setRank(rank++);
                log.info("rank {}",rank);
                if (rank <12) {
                    log.info("user {}",result.getUser());
                    User user = result.getUser();
                    if(user.getRound().equals(Constance.ROUND_1)) {
                        user.setRound(Constance.ROUND_2);
                        highestMarkUsers.add(user);
                        highestMarkResults.add(result);
                    }
                }
            }
            //highest marks contester moved to second round
            //model.addAttribute("results", highestMarkResults);
            model.addAttribute("results", results);
            model.addAttribute("round", round);
            userService.saveAll(highestMarkUsers);

            //return "result/round"+round;
        } else  {
            log.info("activeRounds..............{}",activeRounds);
            log.info("activeRounds size..............{}",activeRounds.size());

            if(activeRounds ==  null || activeRounds.size() == 0) {
                return "redirect:"+"/participant/round/"+round;
            }
        }


        return "result/round"+round;
    }

    @GetMapping("delete")
    public String deleteResult(Model model) {
        resultService.delete();
        return "redirect:"+"/";
    }


    @GetMapping("send/{round}")
    public String sendResult(@PathVariable("round") Integer round ,Model model) {

        Round3AnswerResponse response = new Round3AnswerResponse();
        response.setMessage("result");


        try {
            List<ActiveRound> rounds = activeRoundService.findAll();
            if(rounds.get(0).getRound() == 2) {
                rabbitMQTemplate.convertAndSend(marketDataExchange.getName(), RabbitMQConfig.ROUTING_PATTERN_ROUND2_QUESTION, response);

            } else if(rounds.get(0).getRound() == 3) {
                rabbitMQTemplate.convertAndSend(marketDataExchange.getName(), RabbitMQConfig.ROUTING_PATTERN_ROUND3_QUESTION, response);

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return "redirect:"+"/success";
    }


    @GetMapping("display/{round}")
    public String getResult(@PathVariable("round") Integer round ,Model model) {

        TimerResponse response = new TimerResponse();
        response.setMessage("result");

        try {
            log.info("active round: {} ",round);
            if(round == 1) {
                rabbitMQTemplate.convertAndSend(marketDataExchange.getName(), RabbitMQConfig.ROUTING_PATTERN_ROUND1_RESULT, response);
                log.info("message send to : {} ",RabbitMQConfig.ROUTING_PATTERN_ROUND1_RESULT);
            } else if(round == 2) {
                rabbitMQTemplate.convertAndSend(marketDataExchange.getName(), RabbitMQConfig.ROUTING_PATTERN_ROUND2_QUESTION, response);
                log.info("message send to : {} ",RabbitMQConfig.ROUTING_PATTERN_ROUND2_QUESTION);
            } else if(round == 3) {
                rabbitMQTemplate.convertAndSend(marketDataExchange.getName(), RabbitMQConfig.ROUTING_PATTERN_ROUND3_QUESTION, response);
                log.info("message send to : {} ",RabbitMQConfig.ROUTING_PATTERN_ROUND3_QUESTION);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return "redirect:"+"/success";
    }


}
