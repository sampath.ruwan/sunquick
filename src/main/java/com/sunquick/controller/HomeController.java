package com.sunquick.controller;


import com.sunquick.domain.ActiveRound;
import com.sunquick.domain.Question;
import com.sunquick.dto.response.Round1Question;
import com.sunquick.modelmapper.ModelMapper;
import com.sunquick.service.ActiveRoundService;
import com.sunquick.service.QuestionService;
import com.sunquick.service.ResultService;
import com.sunquick.util.CurrentUser;
import com.sunquick.util.Role;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Slf4j
@Controller
public class HomeController {

    @Autowired
    private QuestionService questionService;

    @Autowired
    private ActiveRoundService activeRoundService;

    @Autowired
    private ResultService resultService;

    @Autowired
    private ModelMapper modelMapper;


    @GetMapping("/")
    public String welcome(Model model, RedirectAttributes redir, HttpServletRequest request) {
        //model.addAttribute("url", new PaymentUrl());

        final CurrentUser currentUser = (CurrentUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        log.info("current user role = {}", currentUser.getRole());
        log.info("check current user role = {}", currentUser.getRole().equals(Role.ADMIN));

        List<ActiveRound> rounds = activeRoundService.findAll();
        log.info("rounds {}", rounds);
        //QuestionAnswerResponse questionAnswerResponse = new QuestionAnswerResponse();
        Round1Question round1Question = new Round1Question();
        List<Round1Question.QuestionData> round1Questions = null;


        if (currentUser.getRole().equals(Role.ADMIN)) {
            return "redirect:participant/list";

        } else if (currentUser.getRole().equals(Role.USER)) {

            if (rounds != null && rounds.size() > 0) {

                if (rounds.get(0).getRound() != currentUser.getUser().getRound()) {
                    redir.addFlashAttribute("error", true);
                    redir.addFlashAttribute("errorMessage", "Active round and Contester hold round mismatch ");
                    return "redirect:" + "/error";
                }

                if (rounds.get(0).getRound() == 2) {
                    return "redirect:" + "question/round2";
                }
                if (rounds.get(0).getRound() == 3) {
                    return "redirect:" + "question/round3";
                }
                List<Question> questions = questionService.questionsByRound(rounds.get(0).getRound());

                round1Questions = modelMapper.map(questions, Round1Question.QuestionData.class);
                round1Question.setQuestions(round1Questions);

            } else {
                redir.addFlashAttribute("error", true);
                redir.addFlashAttribute("errorMessage", "Wait till the game start");
                return "redirect:" + "/error";
            }

            log.info("questionAnswerResponse {}", round1Question);

            model.addAttribute("questionAnswerResponse", round1Question);
            return "home";

        }
        if (currentUser.getRole().equals(Role.MANAGER)) {
            //return "redirect:participant/list";

            if (rounds != null && rounds.size() > 0) {

                if (rounds.get(0).getRound() == 2) {
                    return "redirect:" + "question/round2";
                }
                if (rounds.get(0).getRound() == 3) {
                    return "redirect:" + "question/round3";
                }
                List<Question> questions = questionService.questionsByRound(rounds.get(0).getRound());

                round1Questions = modelMapper.map(questions, Round1Question.QuestionData.class);
                round1Question.setQuestions(round1Questions);

            } else {
                redir.addFlashAttribute("error", true);
                redir.addFlashAttribute("errorMessage", "Wait till the game start");
                return "redirect:" + "/error";
            }

            model.addAttribute("questionAnswerResponse", round1Question);
            return "home";


        } else {

            if (rounds != null && rounds.size() > 0) {

                if (rounds.get(0).getRound() == 1) {

                    return "redirect:" + "result/round/1";
                }
                if (rounds.get(0).getRound() == 2) {
                    return "redirect:" + "participant/round/2";
                    //return "redirect:" + "question/round2";
                }
                if (rounds.get(0).getRound() == 3) {
                    return "redirect:" + "participant/round3";
                }

            }
            return "redirect:" + "result/round/1";
        }
    }


}
