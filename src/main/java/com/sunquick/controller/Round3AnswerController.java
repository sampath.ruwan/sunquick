package com.sunquick.controller;

import com.sunquick.config.RabbitMQConfig;
import com.sunquick.domain.Answer;
import com.sunquick.domain.Round3Answer;
import com.sunquick.dto.request.Round3AnswerRequest;
import com.sunquick.dto.response.Round3AnswerResponse;
import com.sunquick.service.AnswerService;
import com.sunquick.service.QuestionService;
import com.sunquick.service.Round3AnswerService;
import com.sunquick.service.impl.Round3AnswerServiceImpl;
import com.sunquick.util.CurrentUser;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
public class Round3AnswerController {

    @Autowired
    private RabbitTemplate rabbitMQTemplate;

    @Autowired
    private TopicExchange marketDataExchange;

    /*@Autowired
    private DirectExchange marketDataExchange;*/

    @Autowired
    private AnswerService answerService;

    @Autowired
    private QuestionService questionService;

    @Autowired
    private Round3AnswerService round3AnswerService;



    @PostMapping("/answer/submit/round3")
    @ResponseBody
    public Round3AnswerResponse submitRound3Answer(@RequestBody Round3AnswerRequest round3AnswerRequest) {

        log.info("request round3 {}",round3AnswerRequest);

        final CurrentUser currentUser = (CurrentUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        log.info("current user = {}",currentUser.getUsername());
        log.info("current user role = {}",currentUser.getRole());

        Answer answer = answerService.findById(round3AnswerRequest.getAnswerId());

        Round3AnswerResponse response = new Round3AnswerResponse();
        response.setMessage("success");
        response.setAnswerId(round3AnswerRequest.getAnswerId());
        response.setQuestionId(round3AnswerRequest.getQuestionId());
        response.setUserId(currentUser.getUser().getId());
        response.setStatus("answered");
        if(answer != null) {
            response.setNumber(answer.getNumber());
        }

        Round3Answer round3Answer = new Round3Answer();
        round3Answer.setUser(currentUser.getUser());
        round3Answer.setQuestion(questionService.findQuestionById(round3AnswerRequest.getQuestionId()));
        round3Answer.setAnswer(answerService.findById(round3AnswerRequest.getAnswerId()));
        round3AnswerService.save(round3Answer);
        try {
            rabbitMQTemplate.convertAndSend(marketDataExchange.getName(), RabbitMQConfig.ROUTING_PATTERN_ROUND3_QUESTION, response);
        } catch (Exception e) {
            e.printStackTrace();
        }


        return response;
    }
}
