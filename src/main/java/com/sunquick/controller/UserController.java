package com.sunquick.controller;

import com.sunquick.domain.ActiveRound;
import com.sunquick.domain.User;
import com.sunquick.dto.response.Round3UserResponse;
import com.sunquick.service.ActiveRoundService;
import com.sunquick.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;
import java.util.Optional;

@Slf4j
@Controller
@RequestMapping("/participant/")
public class UserController {

    @Autowired
    private UserService userService;


    @Autowired
    private ActiveRoundService activeRoundService;

    private final String UPLOAD_DIR = "uploads/";

    @GetMapping("/register")
    public String viewRegisterForm(Model model) {
        model.addAttribute("user", new User());
        return "register";
    }


    @PostMapping("/register")
    public String register(@RequestParam("file") MultipartFile file, @Valid User user, BindingResult result, Model model) {
        log.info("Registering user {}",user);
        log.info("result.hasErrors {}",result.hasErrors());
        if (result.hasErrors()) {
            return "register";
        }


        // check if file is empty
        /*if (file.isEmpty()) {
            attributes.addFlashAttribute("message", "Please select a file to upload.");
            return "redirect:/";
        }*/

        // normalize the file path
        String fileName = StringUtils.cleanPath(file.getOriginalFilename());

        log.info("fileName {}",fileName);

        if(!file.isEmpty()) {
            user.setPath(UPLOAD_DIR + fileName);
            // save the file on the local file system
            try {
                Path path = Paths.get(UPLOAD_DIR + fileName);
                Files.copy(file.getInputStream(), path, StandardCopyOption.REPLACE_EXISTING);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        userService.save(user);

        //save to database

        return "redirect:login";
    }


    @GetMapping("create")
    public String viewParticipantForm(Model model) {
        model.addAttribute("user", new User());
        return "participant/create";
    }

    @PostMapping("create")
    public String createParticipantForm(@RequestParam("file") MultipartFile file, @Valid User user, BindingResult result, Model model) {

        log.info("Registering user {}",user);

        log.info("result.hasErrors {}",result.hasErrors());
        if (result.hasErrors()) {
            return "participant/create";
        }

        if(user.getId() != null) {
            //need to check other users have same name
        } else {
            if(user.getPassword() == null || user.getPassword().isEmpty()) {
                result.reject("password", "Password may not be blank");
                return "participant/create";
            }
            Optional<User> user1 = userService.getUserByUserName(user.getUsername());
            if(user1.isPresent()) {
                result.reject("username.exists", "User Name already exists");
            }
        }


        // normalize the file path
        String fileName = StringUtils.cleanPath(file.getOriginalFilename());

        log.info("fileName {}",fileName);
        log.info("fileName {}",fileName);

        if(!file.isEmpty()) {
            user.setPath(UPLOAD_DIR + fileName);
            // save the file on the local file system
            try {
                Path path = Paths.get(UPLOAD_DIR + fileName);
                Files.copy(file.getInputStream(), path, StandardCopyOption.REPLACE_EXISTING);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        userService.save(user);
        return "redirect:list";
    }

    @GetMapping("edit/{id}")
    public String edit(@PathVariable("id") long id, Model model) {
        User user = userService.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid Id:" + id));
        model.addAttribute("user", user);

        log.info("redirect:view  {}",id);

        return "participant/create";
    }

    @GetMapping("list")
    public String list(Model model) {
        log.info("list:view ");
        List<ActiveRound> activeRounds = activeRoundService.findAll();
        if(activeRounds != null && activeRounds.size() > 0) {
            model.addAttribute("users", userService.findByRound(activeRounds.get(0).getRound()));
        }else {
            model.addAttribute("users", userService.findAll());
        }

        return "participant/list";
    }


    @GetMapping("delete/{id}")
    public String delete(@PathVariable("id") long id, Model model) {

        log.info("deleting user with id {}",id);


        User user = userService.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid drug Id:" + id));
        userService.delete(user);
        model.addAttribute("users", userService.findAll());
        return "participant/list";
    }

    @GetMapping("round/{round}")
    public String viewResult(@PathVariable("round") Integer round ,Model model) {
        log.info("round vise list:view ");
        model.addAttribute("users", userService.findByRound(round));
        return "participant/list";

    }

    @GetMapping("round3")
    public String round3User(Model model) {
        log.info("round 3 list:view ");

        Round3UserResponse round3UserResponse = new Round3UserResponse();
        List<User> users = userService.findByRound(3);

        log.info("users size {} ",users.size());

        for(int i=0; i <users.size(); i++) {

            if(i==0) {
                round3UserResponse.setUsername1(users.get(i).getUsername());
                round3UserResponse.setPath1(users.get(i).getPath());
            } else if(i==1) {
                round3UserResponse.setUsername2(users.get(i).getUsername());
                round3UserResponse.setPath2(users.get(i).getPath());
            } else if(i==2) {
                round3UserResponse.setUsername3(users.get(i).getUsername());
                round3UserResponse.setPath3(users.get(i).getPath());
            } else if(i==3) {
                round3UserResponse.setUsername4(users.get(i).getUsername());
                round3UserResponse.setPath4(users.get(i).getPath());
            } else if(i==4) {
                round3UserResponse.setUsername5(users.get(i).getUsername());
                round3UserResponse.setPath5(users.get(i).getPath());
            }
        }

        log.info("round3UserResponse {} ",round3UserResponse);


        model.addAttribute("round3UserResponse", round3UserResponse);

        log.info("round3UserResponse {} ",round3UserResponse);

        return "participant/round3";

    }
}
