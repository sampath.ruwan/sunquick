package com.sunquick.controller;

import com.sunquick.config.RabbitMQConfig;
import com.sunquick.domain.*;
import com.sunquick.dto.request.QuestionSecondRound;
import com.sunquick.dto.response.QuestionAnswerResponse;
import com.sunquick.dto.response.QuestionRound3Response;
import com.sunquick.dto.response.Round3AnswerResponse;
import com.sunquick.dto.response.TimerResponse;
import com.sunquick.modelmapper.ModelMapper;
import com.sunquick.service.ActiveRoundService;
import com.sunquick.service.QuestionService;
import com.sunquick.service.ResultService;
import com.sunquick.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Controller
@RequestMapping("/question/")
public class QuestionController {

    @Autowired
    private QuestionService questionService;

    @Autowired
    private ActiveRoundService activeRoundService;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private RabbitTemplate rabbitMQTemplate;

    /*@Autowired
    private DirectExchange marketDataExchange;*/

    @Autowired
    private TopicExchange marketDataExchange;

    @Autowired
    private ResultService resultService;

    @Autowired
    private UserService userService;

    @GetMapping("create")
    public String showCreateForm(Model model) {
        model.addAttribute("question", new Question());
        return "question/create";
    }

    @PostMapping("create")
    public String register(@Valid @ModelAttribute("question") Question question, BindingResult result, Model model) {
        log.info("Registering Question  {}",question);
        if (result.hasErrors()) {
            return "question/create";
        }
        Question questionPersisted = questionService.save(question);
        log.info("Redirecting Question {}",questionPersisted);

        //questionPersisted.getError() != null
        if(questionPersisted == null) {
            model.addAttribute("question", questionPersisted);
            return "question/create";
        } else {

            log.info("redirect:list {}",questionPersisted.getId());

            return "redirect:list";
        }
    }

    @GetMapping("list")
    public String list(Model model) {
        log.info("list:view ");
        model.addAttribute("questions", questionService.findAll());
        return "question/list";
    }

    @GetMapping("round/{round}")
    public String list(@PathVariable("round") Integer round, Model model) {
        log.info("round:view {}",round);

        /*List<Question> questions = null;
        if(round != null) {
            questions = questionService.questionsByRound(round);
        } else {
            questions = questionService.findAll();
        }
        model.addAttribute("questions", questions);
        */

        log.info("requesting question");

        List<ActiveRound> rounds = activeRoundService.findAll();

        log.info("rounds question {}",rounds);

        if(rounds != null && rounds.size() > 0) {

            List<Question> questions = questionService.questionsByRound(rounds.get(0).getRound());

            QuestionAnswerResponse questionAnswerResponse = new QuestionAnswerResponse();
            questionAnswerResponse.setQuestions(questions);

            model.addAttribute("questionAnswerResponse", questionAnswerResponse);
        } else {
            model.addAttribute("error", "Please select valid question round");
        }

        /*Load to queue*/

        TimerResponse response = new TimerResponse();
        response.setMessage("question");

        try {
            if(round == 3) {
                rabbitMQTemplate.convertAndSend(marketDataExchange.getName(), RabbitMQConfig.ROUTING_PATTERN_ROUND3_QUESTION, response);
            } else {
                rabbitMQTemplate.convertAndSend(marketDataExchange.getName(), RabbitMQConfig.ROUTING_PATTERN_ROUND2_QUESTION, response);
            }

            log.info("intro send question to queue ");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "question/send";
    }

    @GetMapping("view/{id}")
    public String showUpdateForm(@PathVariable("id") long id, Model model) {

        log.info("redirect:view  {}",id);

        Question question = questionService.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid Id:" + id));
        model.addAttribute("question", question);

        log.info("redirect:view  {}",id);

        return "question/list";
    }

    @GetMapping("edit/{id}")
    public String edit(@PathVariable("id") long id, Model model) {
        Question question = questionService.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid Id:" + id));
        model.addAttribute("question", question);

        log.info("redirect:view  {}",id);

        return "question/create";
    }


    @GetMapping("delete/{id}")
    public String delete(@PathVariable("id") long id, Model model) {
        Question url = questionService.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid drug Id:" + id));
        questionService.delete(url);
        model.addAttribute("questions", questionService.findAll());
        return "question/list";
    }


    @RequestMapping(value="create", params={"addAnswer"})
    public String addRow(@ModelAttribute("question") Question question, BindingResult result, Model model) {
        question.getAnswers().add(new Answer());
        return "question/create";
    }

    @RequestMapping(value="create", params={"removeRow"})
    public String removeRow(@ModelAttribute("question") Question question, BindingResult result, Model model, HttpServletRequest req) {
        final Integer rowId = Integer.valueOf(req.getParameter("removeRow"));
        question.getAnswers().remove(rowId.intValue());
        return "question/create";
    }

    @GetMapping("paper/{round}")
    public String retrieve(@PathVariable("round")Integer round,Model model) {

        log.info("requesting question {}",round);

        List<User> users= new ArrayList<>();

        List<ActiveRound> rounds = activeRoundService.findAll();

        log.info("rounds question {}",rounds);

        List<Question> questions = new ArrayList<>();

        if(rounds != null && rounds.size() > 0) {

            if(rounds.get(0).getRound() == 2) {

                questions = questionService.questionsByRound(rounds.get(0).getRound());

            } else if(rounds.get(0).getRound() == 3) {
                //Load round three contesters

                users = userService.findByRound(rounds.get(0).getRound());

            }


            QuestionAnswerResponse questionAnswerResponse = new QuestionAnswerResponse();
            questionAnswerResponse.setQuestions(questions);

            log.info("user size {}",users.size());

            if(users != null && users.size() > 0) {
                questionAnswerResponse.setUsers(users);
            }

            model.addAttribute("questionAnswerResponse", questionAnswerResponse);
        } else {
            model.addAttribute("error", "Please select valid question round");
        }
        return "question/paper";
    }

    /*find active round and pick the question which are not send already to user*/
    @GetMapping("select")
    public String questionForSecondAndThird(Model model) {

        log.info("requesting question");

        List<ActiveRound> rounds = activeRoundService.findAll();

        log.info("rounds question {}",rounds);

        if(rounds != null && rounds.size() > 0) {

            List<Question> questions = questionService.questionsByRound(rounds.get(0).getRound());

            QuestionAnswerResponse questionAnswerResponse = new QuestionAnswerResponse();
            questionAnswerResponse.setQuestions(questions);

            model.addAttribute("questionAnswerResponse", questionAnswerResponse);
        } else {
            model.addAttribute("error", "Please select valid question round");
        }
        return "question/send";
    }

    /*After send the question to the user question sent update as true and sent time will update*/
    @PostMapping("send")
    public String send(@ModelAttribute("questionAnswerResponse") QuestionAnswerResponse questionAnswerResponse, BindingResult result, Model model, RedirectAttributes redir) {

        log.info("request {}",questionAnswerResponse);
        List<Question> questionsToSend = new ArrayList<>();

        if(questionAnswerResponse.getQuestions() != null && questionAnswerResponse.getQuestions().size() > 0) {
            List<Question> questions = questionAnswerResponse.getQuestions().stream().filter(question -> question.getIsSet()).collect(Collectors.toList());

            log.info("questions {}",questions);

            if(questions != null && questions.size() > 0) {
                log.info("questions {}",questions.size());

                for (Question questionToSend:questions) {
                    Question question = questionService.findQuestionById(questionToSend.getId());

                    question.setSentTime(LocalDateTime.now());
                    question.setSent(Boolean.TRUE);
                    //question.setSent(Boolean.TRUE);
                    questionsToSend.add(question);
                }


                log.info("questionsToSend size {}",questionsToSend.size());

                //QuestionSecondRound questionForSecondRound = new QuestionSecondRound();

                List<QuestionSecondRound> questionSecondRounds = modelMapper.map(questionsToSend,QuestionSecondRound.class);

                List<ActiveRound> rounds = activeRoundService.findAll();

                try {
                    if(rounds.get(0).getRound() == 3) {
                        rabbitMQTemplate.convertAndSend(marketDataExchange.getName(), RabbitMQConfig.ROUTING_PATTERN_ROUND3_QUESTION, questionSecondRounds);
                    } else {
                        rabbitMQTemplate.convertAndSend(marketDataExchange.getName(), RabbitMQConfig.ROUTING_PATTERN_ROUND2_QUESTION, questionSecondRounds);
                    }

                    log.info("send question to queue size {}",questionSecondRounds);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                questionService.saveAll(questionsToSend);



            }
        }
        redir.addFlashAttribute("error", true);
        redir.addFlashAttribute("errorMessage", "Question send success");
        return "redirect:"+"/success";
    }

    @GetMapping("round2")
    public String retrieveRound2(Model model) {

        log.info("round2 requesting question ");

        List<User> users= new ArrayList<>();

        List<ActiveRound> rounds = activeRoundService.findAll();

        log.info("rounds question {}",rounds);

        if(rounds != null && rounds.size() > 0) {

            //get latest send question
            Question question = questionService.findByRound(2);
            model.addAttribute("question", question);

            /*if(rounds != null && rounds.get(0).getRound() == 2) {
                //Load round two contesters
                users = userService.findByRound(rounds.get(0).getRound());
            }

            QuestionRound3Response questionRound3Response = new QuestionRound3Response();

            log.info("user size {}",users.size());

            if(users != null && users.size() > 0) {

                List<QuestionRound3Response.UserData> questionRound3Users = modelMapper.map(users,QuestionRound3Response.UserData.class);
                questionRound3Response.setUsers(questionRound3Users);

                model.addAttribute("questionAnswerResponse", questionRound3Response);
            }

            List<Result> results = resultService.findByRound(3);

            int rank =1;
            for (Result result:results) {
                result.setRank(rank++);
            }

            List<QuestionRound3Response.ResultData> resultsData = modelMapper.map(results,QuestionRound3Response.ResultData.class);
            questionRound3Response.setResults(resultsData);*/



        } else {

            model.addAttribute("error", "Please select valid question round");

        }
        return "question/round2";
    }

    @GetMapping("round3")
    public String retrieveRound3(Model model) {

        log.info("round3 requesting question ");

        List<User> users= new ArrayList<>();

        List<ActiveRound> rounds = activeRoundService.findAll();

        log.info("rounds question {}",rounds);

        if(rounds != null && rounds.size() > 0) {

            if(rounds.get(0).getRound() == 3) {
                //Load round three contesters
                users = userService.findByRound(rounds.get(0).getRound());
           }

           QuestionRound3Response questionRound3Response = new QuestionRound3Response();

            log.info("user size {}",users.size());

            if(users != null && users.size() > 0) {

                for (int i=0; i < users.size(); i++) {

                    users.get(i).setTempClass("user_"+i);
                }

                List<QuestionRound3Response.UserData> questionRound3Users = modelMapper.map(users,QuestionRound3Response.UserData.class);
                questionRound3Response.setUsers(questionRound3Users);

                model.addAttribute("questionAnswerResponse", questionRound3Response);
            }

            List<Result> results = resultService.findByRound(3);

            int rank =1;
            for (Result result:results) {
                result.setRank(rank++);
            }

            List<QuestionRound3Response.ResultData> resultsData = modelMapper.map(results,QuestionRound3Response.ResultData.class);
            questionRound3Response.setResults(resultsData);

            Question question = questionService.findByRound(3);
            model.addAttribute("question", question);

        } else {

            model.addAttribute("error", "Please select valid question round");

        }




        return "question/round3";
    }

    @GetMapping("send/round3")
    public String sendToRound3(Model model) {

        Round3AnswerResponse response = new Round3AnswerResponse();
        response.setMessage("round3");

        try {
            rabbitMQTemplate.convertAndSend(marketDataExchange.getName(), RabbitMQConfig.ROUTING_PATTERN_ROUND3_QUESTION, response);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return "redirect:"+"/success";
    }


    @GetMapping("update")
    public String updateCurrentRoundQuestion(Model model) {

        List<ActiveRound> rounds = activeRoundService.findAll();

        log.info("round {}",rounds);

        if(rounds != null && rounds.size() > 0) {

            List<Question> questions = questionService.questionsByRound(rounds.get(0).getRound());

            if(questions != null && questions.size() > 0) {

                for (Question question : questions) {
                    question.setSent(Boolean.TRUE);
                }

                questionService.saveAll(questions);

            }


        }

        return "redirect:"+"/success";
    }


    @GetMapping("intro")
    public String showIntro(Model model) {

        TimerResponse response = new TimerResponse();
        //response.setMessage("intro");

        List<ActiveRound> rounds = activeRoundService.findAll();

        try {

            if(rounds != null && rounds.size() > 0) {
                if(rounds.get(0).getRound() .equals(3)) {
                    response.setMessage("intro3");
                    rabbitMQTemplate.convertAndSend(marketDataExchange.getName(), RabbitMQConfig.ROUTING_PATTERN_ROUND3_QUESTION, response);
                } else if(rounds.get(0).getRound() .equals(2)){
                    response.setMessage("intro2");
                    rabbitMQTemplate.convertAndSend(marketDataExchange.getName(), RabbitMQConfig.ROUTING_PATTERN_ROUND2_QUESTION, response);
                } else {
                    return "question/intro1";
                }
            } else {
                return "question/intro1";
            }


            log.info("intro send question to queue ");
        } catch (Exception e) {
            e.printStackTrace();
        }

        return "redirect:"+"/success";
    }

}
