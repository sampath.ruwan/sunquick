package com.sunquick.controller;

import com.sunquick.config.RabbitMQConfig;
import com.sunquick.dto.response.TimerResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Slf4j
@Controller
@RequestMapping("/timer")
public class TimerController {


    @Autowired
    private RabbitTemplate rabbitMQTemplate;

    @Autowired
    private TopicExchange marketDataExchange;

    @Value("${timer.unit}")
    private String unit;

    @Value("${timer.val}")
    private String value;

    @GetMapping("/")
    public String sendTimer(Model model) {

        TimerResponse response = new TimerResponse();
        response.setMessage("timer");

        try {
            rabbitMQTemplate.convertAndSend(marketDataExchange.getName(), RabbitMQConfig.ROUTING_PATTERN_TIMER, response);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return "redirect:"+"/question/intro";
    }

    /*@RequestParam("unit") String unit,@RequestParam("value") Integer value*/

    @GetMapping("/show")
    public String timer( Model model) {

        log.info("unit {}",unit);
        log.info("value {}",value);

        model.addAttribute("unit",unit);
        model.addAttribute("value",value);

        /*TimerResponse response = new TimerResponse();
        response.setMessage("timer");

        try {
            rabbitMQTemplate.convertAndSend(marketDataExchange.getName(), RabbitMQConfig.ROUTING_PATTERN_TIMER, response);
        } catch (Exception e) {
            e.printStackTrace();
        }*/

        return "timer/timer";
    }
}
