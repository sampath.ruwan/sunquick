package com.sunquick.modelmapper;

public interface ModelMappingAware {
    Class<?> getDestinationType();
}
