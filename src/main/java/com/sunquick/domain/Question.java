package com.sunquick.domain;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Entity
@Table
public class Question extends CreateModifyAwareBaseEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank(message = "Question Name may not be blank")
    @Length(max = 1000, message = "Question length exceed")
    private String name;


    @NotNull(message = "Question Round may not null")
    private Integer round;

    //for second round selection
    @Transient
    private Boolean isSet;

    private Boolean sent=Boolean.FALSE;

    //private Boolean lastSent=Boolean.FALSE;

    private LocalDateTime sentTime;



    @OneToMany(mappedBy = "question",cascade = {CascadeType.MERGE,CascadeType.PERSIST,CascadeType.REMOVE})
    private List<Answer> answers = new ArrayList<>();

    @Override
    public String toString() {
        return "Question{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", round=" + round +
                ", isSet=" + isSet +
                '}';
    }
}
