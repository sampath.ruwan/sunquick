package com.sunquick.domain;

import com.sunquick.util.Role;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter
@Entity
@Table
public class User extends CreateModifyAwareBaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank(message = "User Name may not be blank")
    private String username;

    //@NotBlank(message = "Password may not be blank")
    private String password;

    private String email;

    private String path;

    private Boolean enabled;

    @Column(name = "role", nullable = false)
    @Enumerated(EnumType.STRING)
    private Role role;

    private Integer round;

    @Transient
    private String tempClass;

    @Override
    public String toString() {
        return "User{" +
                "id='" + id + '\'' +
                "userName='" + username + '\'' +
                '}';
    }
}
