package com.sunquick.domain;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;


@Getter
@Setter
@Entity
@Table
public class Round3Answer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne()
    @JoinColumn(name="question_id", nullable = false)
    private Question question;

    @OneToOne()
    @JoinColumn(name="answer_id", nullable = false)
    private Answer answer;

    @OneToOne()
    @JoinColumn(name="user_id", nullable = false)
    private User user;

    @Override
    public String toString() {
        return "Round3Answer{" +
                "id=" + id +
                ", question=" + question +
                ", answer=" + answer +
                ", user=" + user +
                '}';
    }
}
