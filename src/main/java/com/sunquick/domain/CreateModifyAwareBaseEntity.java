package com.sunquick.domain;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;
import java.time.LocalDate;
import java.time.LocalDateTime;


@EntityListeners({AuditingEntityListener.class})
@MappedSuperclass
public abstract class CreateModifyAwareBaseEntity {

    @CreatedBy
    private Long createdUserId;
    @LastModifiedBy
    private Long lastModifiedUserId;
    @CreatedDate
    private LocalDate createdDate=LocalDate.now();
    @LastModifiedDate
    private LocalDate lastModifiedDate;

    public CreateModifyAwareBaseEntity() {
    }

    public Long getCreatedUserId() {
        return this.createdUserId;
    }

    public Long getLastModifiedUserId() {
        return this.lastModifiedUserId;
    }

    public void setCreatedUserId(Long createdUserId) {
        this.createdUserId = createdUserId;
    }

    public void setLastModifiedUserId(Long lastModifiedUserId) {
        this.lastModifiedUserId = lastModifiedUserId;
    }

    public LocalDate getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(LocalDate createdDate) {
        this.createdDate = createdDate;
    }

    public LocalDate getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(LocalDate lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

}
