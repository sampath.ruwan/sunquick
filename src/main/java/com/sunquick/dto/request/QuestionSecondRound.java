package com.sunquick.dto.request;

import com.sunquick.domain.Question;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class QuestionSecondRound implements Serializable {


    private Long id;

    private String name;

    //public QuestionData question;

    List<AnswerData> answers;

    @Data
    public static class AnswerData {

        private Long id;

        private String name;

        private String number;
    }


}
