package com.sunquick.dto.request;

import lombok.Data;

import java.util.List;

@Data
public class CorrectAnswerQuestionRequest {

    private Long id;

    private String name;

    private Integer round;

    //for second round selection
    private Boolean isSet;

    private List<AnswerData> answers;

    @Data
    public static class AnswerData {

        private Long id;

        private String name;

        private String number;



    }
}
