package com.sunquick.dto.request;

import com.sunquick.domain.Answer;
import com.sunquick.domain.Question;
import com.sunquick.modelmapper.ModelMappingAware;
import lombok.Data;

import java.util.List;

@Data
public class AnswerSubmitRequest {

    List<ParticipantAnswerData> participantAnswers;

    private Long userId;

    @Data
    public static class ParticipantAnswerData {

        private QuestionData question;

        private List<AnswerData> answers;

    }

    @Data
    public static class QuestionData implements ModelMappingAware {

        private Long id;

        @Override
        public Class<?> getDestinationType() {
            return Question.class;
        }

    }

    @Data
    public static class AnswerData implements ModelMappingAware {

        private Long id;

        @Override
        public Class<?> getDestinationType() {
            return Answer.class;
        }

    }
}
