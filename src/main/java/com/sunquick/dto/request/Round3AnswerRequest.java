package com.sunquick.dto.request;

import lombok.Data;

@Data
public class Round3AnswerRequest {

    private Long questionId;

    private Long answerId;

    @Override
    public String toString() {
        return "Round3AnswerRequest{" +
                "questionId=" + questionId +
                ", answerId=" + answerId +
                '}';
    }
}
