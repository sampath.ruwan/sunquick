package com.sunquick.dto.response;

import lombok.Data;

import java.util.List;

@Data
public class Round1Question {

    public List<QuestionData> questions;

    @Data
    public static class QuestionData {

        private Long id;

        private String name;

        private Integer round;

        //private Boolean isSet;

        private List<AnswerData> answers;

    }

    @Data
    public static class AnswerData {

        private Long id;

        private String name;

        private String number;

        private Boolean isSet;

    }

}
