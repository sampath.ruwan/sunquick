package com.sunquick.dto.response;

import com.sunquick.domain.Question;
import com.sunquick.domain.User;
import lombok.Data;

import java.util.List;

@Data
public class QuestionRound3Response {


    public List<UserData> users;


    @Override
    public String toString() {
        return "QuestionAnswerResponse{" +
                "users=" + users+
                '}';
    }

    @Data
    public static class UserData {

        private Long id;

        private String username;

        private String path;

        private String tempClass;

    }

    private List<ResultData> results;



    @Data
    public static class ResultData {

        private Long id;

        private UserData user;

        private Integer mark;

        private Integer rank;

    }
}
