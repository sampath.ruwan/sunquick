package com.sunquick.dto.response;

import lombok.Data;

@Data
public class Round3UserResponse {

    private String path1;
    private String username1;
    private String path2;
    private String username2;
    private String path3;
    private String username3;
    private String path4;
    private String username4;
    private String path5;
    private String username5;

    /*private UserData user1;
    private UserData user2;
    private UserData user3;
    private UserData user4;
    private UserData user5;

    @Data
    public static class UserData {

        private String username;
        private String path;

    }*/

}
