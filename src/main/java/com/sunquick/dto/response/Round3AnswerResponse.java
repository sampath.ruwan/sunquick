package com.sunquick.dto.response;

import com.sunquick.domain.Result;
import com.sunquick.domain.User;
import lombok.Data;

import java.util.List;

@Data
public class Round3AnswerResponse {

    private String message;

    private Long questionId;

    private Long answerId;

    private Long userId;

    private List<UserData> users;

    private String number;

    private List<ResultData> results;

    private String status;

    private List<UserAnswerData> userAnswers;



    @Data
    public static class ResultData {

        private Long id;

        private UserData user;

        private Integer mark;

        private Integer rank;

    }

    @Data
    public static class UserData {

        private Long id;

        private String username;

        private String path;



    }

    @Data
    public static class UserAnswerData {

        private AnswerData answer;

        private UserData user;


    }

    @Data
    public static class AnswerData {

        private Long id;

        private String number;




    }
}
