package com.sunquick.dto.response;

import lombok.Data;

@Data
public class AnswerSubmitResponse {

    private String message="SUCCESS";
}
