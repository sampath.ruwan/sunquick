package com.sunquick.dto.response;

import com.sunquick.domain.Answer;
import com.sunquick.domain.Question;
import com.sunquick.domain.User;
import lombok.Data;

import java.util.List;

@Data
public class QuestionAnswerResponse {

    public List<Question> questions;

    public List<User> users;



}
