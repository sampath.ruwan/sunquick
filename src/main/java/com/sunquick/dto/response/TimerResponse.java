package com.sunquick.dto.response;

import lombok.Data;

@Data
public class TimerResponse {

    private String message;
    private String unit = "s";
    private Integer amountVal = 20;
}
